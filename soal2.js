function reverses(str) {
  if (str.length <= 1) {
    return str;
  } else {
    return (
      str.charAt(str.length - 1) + reverses(str.substring(0, str.length - 1))
    );
  }
}

console.log(reverses("Hello"));
